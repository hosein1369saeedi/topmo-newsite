<!doctype html>
<html lang="en">
  <?php include("block/head.php");?>
  <body>
    <?php include("block/menu.php");?>
    <?php include("block/header.php");?>

    <?php include("block/application.php");?>
    <?php include("block/box.php");?>
    <?php include("block/comment.php");?>
    <?php include("block/wrapper.php");?>
    <?php include("block/pluginnumber.php");?>
    <?php include("block/login.php");?>
    <?php include("block/timeline.php");?>


    <?php include("block/footer.php");?>
    <?php include("block/script.php");?>

  </body>
</html>