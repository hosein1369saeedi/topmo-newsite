<section>
    <div class="application">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <h3>اپلیکیشن مشاوره تاپمو</h3>
                        <div class="row">
                            <div class="col-md text-center">
                                <img src="icon/gallery.png" alt="">
                                <p>پرداخت و شارژ کیف پول</p>
                            </div>
                            <div class="col-md text-center">
                                <img src="icon/reserved.png" alt="">
                                <p>امکان رزرو نوبت مشاوره</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md text-center">
                                <img src="icon/worldwide.png">
                                <p>نمایش مشاوران نزدیک من</p>
                            </div>
                            <div class="col-md text-center">
                                <img src="icon/teamwork.png">
                                <p>نمایش مشاوران آنلاین</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md text-center">
                                <img src="icon/avatar.png">
                                <p>پروفایل مشاوران</p>
                            </div>
                            <div class="col-md text-center">
                                <img src="icon/avatar.png">
                                <p>پروفایل کاربران</p>
                            </div>
                        </div>
                        <div class="row app">
                            <div class="col-md-6 text-center">
                                <a href="http://topmo.ir/">
                                    <img src="image/googleplay.png">
                                </a> 
                            </div>
                            <div class="col-md-6 text-center">
                                <a href="http://topmo.ir/">
                                    <img src="image/appstore.png">
                                </a>
                            </div>
                        </div>
                    </div> 
                    <div class="col-md-4 text-center">
                        <img class="appimg" src="image/app-1.png">
                    </div>
                    <br>
                    <br>
                </div>
            </div>
        </div>
    </div>
</section>
