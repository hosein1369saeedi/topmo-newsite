<div id="carouselExampleIndicators" class="carousel slide my-carousel my-carousel" data-ride="carousel">
  <div class="container">		    
    <h1>تجربه کسانی که به ما اعتماد کردنند</h1>
    <div class="carousel-inner" role="listbox" id="quote-carousel">
      <div class="carousel-item active" >
        <h4>عباس الفتی</h4>
        <h6>مشاور کسب و کار</h6>
        <h5>برای راه اندازی یک سایت اینترنتی از مشاوره تاپمو کمک گرفتم و در حال حاضر یک استارتاپ موفق راه اندازی کرده ام. و همچنان برای اداره استارتاپم به مشورت هام ادامه میدهم</h5>
        <img src="image/1.png">
      </div>
      <div class="carousel-item " >
        <h4>عباس الفتی</h4>
        <h6>مشاور کسب و کار</h6>
        <h5>برای راه اندازی یک سایت اینترنتی از مشاوره تاپمو کمک گرفتم و در حال حاضر یک استارتاپ موفق راه اندازی کرده ام. و همچنان برای اداره استارتاپم به مشورت هام ادامه میدهم</h5>
        <img src="image/1.png">
      </div>
      <div class="carousel-item " >
        <h4>عباس الفتی</h4>
        <h6>مشاور کسب و کار</h6>
        <h5>برای راه اندازی یک سایت اینترنتی از مشاوره تاپمو کمک گرفتم و در حال حاضر یک استارتاپ موفق راه اندازی کرده ام. و همچنان برای اداره استارتاپم به مشورت هام ادامه میدهم</h5>
        <img src="image/1.png">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
    <br><br>
    <ol class="carousel-indicators">
      <li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
      <li data-target="#quote-carousel" data-slide-to="1"></li>
      <li data-target="#quote-carousel" data-slide-to="2"></li>
    </ol>
  </div>
</div>
<script src="js/jquery-comment-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="js/bootstrap-comment.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>